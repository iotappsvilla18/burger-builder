import React, { Component } from 'react';
import Aux from '../../../hoc/Aux';
import Button from '../../UI/Button/Button';


class OrderSummery extends Component {

    componentWillUpdate () {
        console.log ('[OrderSummery] Update');
    }

    render () {
        const ingredientSummery = Object.keys(this.props.ingredients)
        .map(igKey => {
            return (<li key={igKey}>
                        <span style={{textTransform: 'capitalize'}}>{igKey}</span>:
                         {this.props.ingredients[igKey]}</li>
                        );
        });
        return (
            <Aux>
            <h3>Your Order</h3>
            <p>A Delicious Burger with the following Ingredients:</p>
            <ul>    
                {ingredientSummery}   
            </ul>
            <p><strong>Total Price: {this.props.price.toFixed(2)}</strong></p>
            <p>Continue to Checkout?</p>
            <Button btnType="Danger" clicked={this.props.purchaseCanceled}>CANCEL</Button>
            <Button btnType="Success" clicked={this.props.purchaseContinue}>PROCEED</Button>
        </Aux>
        );
    }
}

export default OrderSummery;