import React from 'react';
import classes from './Toolbar.css';
import Logo from '../../Logo/Logo';
import NavigationItems from '../NavigationItems/NavigationItems';
import DrwerToggle from '../SideBar/DrowerToggle/DrwerToggle';




const toolbar = (props) => (
    <header className={classes.Toolbar}>
        <DrwerToggle clicked={props.DrwerToggleClicked}/>
        <div className={classes.Logo}><Logo /></div>
        <nav className={classes.DesktopOnly}>
           <NavigationItems />
        </nav>
    </header>

);

export default toolbar;